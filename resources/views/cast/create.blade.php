@extends('layout.master')

@section('judul')
halaman cast
@endsection

@section('content')
<form action ="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" calss = "form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="integer" name="umur" calss = "form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" calss = "form-control"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection